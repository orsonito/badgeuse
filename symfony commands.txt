PROJECT CREATION
Creation of the project				composer create-project symfony/website-skeleton project-name ^5.*
Creation api res				composer create-project symfony/keleton project-name ^5.*
Update symfony server				symfony self:update

CODESNIFFER
Installation codeSniffer Global			composer global require "squizlabs/php_codesniffer=*"
Installation codeSniffer in the project		composer require "squizlabs/php_codesniffer=*"
Run codeSniffer					./vendor/bin/phpcs -h
Fix errors with codeSniffer			./vendor/bin/phpcbf -h FICHIER§

TESTING
Installation PHPUnit				composer require --dev phpunit/phpunit symfony/test-pack
						php bin/phpunit
Running PHPUnit					php ./vendor/bin/phpunit
						php ./vendor/bin/phpunit tests/folder/TestTest.php
Creation of tests				php bin/console make:test (TestCase)

EASY ADMIN
Creation of easyAdmin				symfony composer req "admin:^3"
Dir						mkdir src\Controller\Admin\
Creation CRUD					php bin/console make:admin:crud

EVENTLISTENER
Implementation subscriber			symfony console make:subscriber TwigEventSubscriber
						(Symfony\Component\HttpKernel\Event\ControllerEvent)

COMMAND						php bin/console app:delete-user 34

SERVER
Start server					symfony server:start -d
Launch server					symfony open:local

WORK WITH APACHE				composer require symfony/apache-pack


CONTROLLER
Create controller 				php bin/console make:controller NameController

ENTITIES
Create entity					php bin/console make:entity Entityname
Create migration				php bin/console doctrine:migrations:diff
Migration from entities to DataBase		php bin/console doctrine:migrations:migrate
(Turn entities to tables)

Create entity from DataBase annotation		php bin/console doctrine:mapping:import App\Entity annotation --path=src/Entity
Create entity from DataBase yml			php bin/console doctrine:mapping:import App\Entity yml --path=src/Entity
Create setter and getter for the entities	php bin/console make:entity --regenerate App


QUERY
Query for the database				php bin/console doctrine:query:sql "SELECT * FROM tableName"