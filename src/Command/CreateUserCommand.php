<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use App\Service\UserService;
use App\Entity\User;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create-user';
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('name',       InputArgument::OPTIONAL, 'The name of the user.')
            ->addArgument('lastName',   InputArgument::OPTIONAL, 'The last name of the user.')
            ->addArgument('email',      InputArgument::OPTIONAL, 'The email of the user.')
            ->addArgument('password',   InputArgument::OPTIONAL, 'The password of the user.')
            ->setDescription('Creates a new user for the badgeuse.')
            ->setHelp('This command allows you to create an user for the badgeuse...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            '',
            '<fg=white;bg=green> =============================================== </>',
            '<fg=white;bg=green;options=bold> G O D   I T S   C R E A T I N G   A N   U S E R </>',
            '<fg=white;bg=green> =============================================== </>',
            '',
        ]);

        $output->writeln('<fg=cyan>Pater noster, qui es in caelis</>');
        $output->writeln('<fg=cyan>sanctificetur Nomen Tuum</>');
        $output->writeln('<fg=cyan>adveniat Regnum Tuum</>');
        $output->writeln('<fg=cyan>fiat voluntas Tua</>');
        $output->writeln('<fg=cyan>sicut in caelo, et in terra</>');
        $output->writeln('<fg=cyan>Panem nostrum cotidianum da nobis hodie</>');
        $output->writeln('<fg=cyan>et dimitte nobis debita nostra</>');
        $output->writeln('<fg=cyan>sicut et nos dimittimus debitoribus nostris</>');
        $output->writeln('<fg=cyan>et ne nos inducas in tentationem</>');
        $output->writeln('<fg=cyan>sed libera nos a Malo</>');

        $name       = $input->getArgument('name');
        $lastName   = $input->getArgument('lastName');
        $email      = $input->getArgument('email');
        $password   = $input->getArgument('password');

        $user = new User();
        isset($name)        ? $user->setName($name)            : $user->setName('Orsonito');
        isset($lastName)    ? $user->setLastName($lastName)    : $user->setLastName('Bonito');
        isset($email)       ? $user->setEmail($email)          : $user->setEmail('email@email.fr');
        isset($password)    ? $user->setPassword($password)    : $user->setPassword('1234');

        if ($this->userService->create($user)) {
            $output->writeln([
                '',
                '<fg=white;options=bold> User ' . $input->getArgument('name') . ' created ! </>',
                '',
            ]);

            $output->writeln([
                '',
                '<fg=white;bg=cyan> =============== </>',
                '<fg=white;bg=cyan;options=bold>  A   M   E   N  </>',
                '<fg=white;bg=cyan> =============== </>',
                '',
            ]);

            return Command::SUCCESS;
        } else {
            throw new \LogicException('I T   I S   T H E   A P O C A L Y P S E');
            return Command::FAILURE;
        }
    }
}
