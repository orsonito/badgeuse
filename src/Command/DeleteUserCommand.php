<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

use App\Service\UserService;
use App\Entity\User;

class DeleteUserCommand extends Command
{
    protected static $defaultName = 'app:delete-user';
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('id', InputArgument::REQUIRED, 'ID for the user to erase FOR THE REAL')
            ->setDescription('Delete an user for the badgeuse.')
            ->setHelp('This command allows you to delete an user for the badgeuse...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            '',
            '<fg=white;bg=green> =============================================== </>',
            '<fg=white;bg=green;options=bold> G O D   I T S   D E L E T I N G   A N   U S E R </>',
            '<fg=white;bg=green> =============================================== </>',
            '',
        ]);

        $output->writeln('<fg=cyan>Ave Maria, gratia plena</>');
        $output->writeln('<fg=cyan>Dominus tecum</>');
        $output->writeln('<fg=cyan>benedicta tu in mulieribus,</>');
        $output->writeln('<fg=cyan>et benedictus fructus ventris tui, Jesus.</>');
        $output->writeln('<fg=cyan>Sancta Maria mater Dei,</>');
        $output->writeln('<fg=cyan>ora pro nobis peccatoribus,</>');
        $output->writeln('<fg=cyan>nunc et in hora mortis nostrae.</>');

        $id     = $input->getArgument('id');
        $user   = new User();
        $user->setId($id);

        $user = $this->userService->read($user);

        if ($this->userService->delete($user)) {
            $output->writeln([
                '',
                '<fg=white;options=bold> User ' . $user->getName() . ' deleted ! </>',
                '',
            ]);

            $output->writeln([
                '',
                '<fg=white;bg=cyan> =============== </>',
                '<fg=white;bg=cyan;options=bold>  A   M   E   N  </>',
                '<fg=white;bg=cyan> =============== </>',
                '',
            ]);

            return Command::SUCCESS;
        } else {
            throw new \LogicException('I T   I S   T H E   A P O C A L Y P S E');
            return Command::FAILURE;
        }
    }
}
