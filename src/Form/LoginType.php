<?php

namespace App\Form;

// FORM CREATION
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST')
            ->add(
                'email', EmailType::class, array(
                'label' => 'Your Email',
                'attr'  => ['class' => 'form-control']
                )
            )
            ->add(
                'password', PasswordType::class, array(
                'label' => 'Your Password',
                'attr'  => ['class' => 'form-control']
                )
            )
            ->add(
                'submit', SubmitType::class, array(
                'label' => 'Log in',
                'attr'  => ['class' => 'btn btn-secondary btn-lg btn-block']
                )
            );
    }
}
