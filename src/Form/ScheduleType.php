<?php

namespace App\Form;

// FORM CREATION
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ScheduleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST')
            ->add(
                'day', ChoiceType::class, array(
                'label'     => 'Select your day',
                'choices'   => [
                    'Monday'    => 'monday',
                    'Tuesday'   => 'tuesday',
                    'Wednesday' => 'wednesday',
                    'Thursday'  => 'thursday',
                    'Friday'    => 'friday'
                ],
                'attr'      => ['class' => 'btn btn-secondary dropdown-toggle']
                )
            )
            ->add(
                'entry_time', TimeType::class, array(
                'label' => 'Select your entry time',
                'attr'  => ['class' => 'form-control']
                )
            )
            ->add(
                'exit_time', TimeType::class, array(
                'label' => 'Select your exit time',
                'attr'  => ['class' => 'form-control']
                )
            )
            ->add(
                'submit', SubmitType::class, array(
                'label' => 'Set my schedule',
                'attr'  => ['class' => 'btn btn-light']
                )
            );
    }
}
