<?php

namespace App\Form;

// FORM CREATION
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST')
            ->add(
                'name', TextType::class, array(
                    'label' => 'Your Name',
                    'attr'  => ['class' => 'form-control']
                )
            )
            ->add(
                'last_name', TextType::class, array(
                    'label' => 'Your Last Name',
                    'attr'  => ['class' => 'form-control']
                )
            )
            ->add(
                'email', EmailType::class, array(
                    'label' => 'Your Email',
                    'attr'  => ['class' => 'form-control']
                )
            )
            ->add(
                'password', PasswordType::class, array(
                    'label' => 'Your Password',
                    'attr'  => ['class' => 'form-control']
                )
            )
            ->add(
                'submit', SubmitType::class, array(
                    'label' => 'Join us',
                    'attr'  => ['class' => 'btn btn-secondary btn-lg btn-block']
                )
            );
    }
}
