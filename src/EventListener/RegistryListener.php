<?php

namespace App\EventListener;

use App\Entity\Registry;
use Doctrine\Persistence\Event\LifecycleEventArgs;

// USE MAILER
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class RegistryListener
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function postPersist(Registry $registry, LifecycleEventArgs $event): void
    {
        // ENVOYER EMAIL S'IL Y A UN RETARD
        if (($registry->getDelayTime()->getTimestamp() + 3600) > 900) {
            $this->sendEmail($registry);
        }
    }

    /**
     * @Route("/email")
     */
    public function sendEmail($registry): void
    {
        $email = (new TemplatedEmail())
            ->from('orsonigt@gmail.com')
            ->to('orsongt@outlook.com')
            ->context(
                [
                    'registryId'        => $registry->getId(),
                    'registryIdUser'    => $registry->getIdUser()->getId(),
                    'registryName'      => $registry->getIdUser()->getName(),
                    'registryDelayTime' => $registry->getDelayTime(),
                    'registryEntryTime' => $registry->getEntryTime(),
                    'scheduleDay'       => false,
                    'schedules'         => $registry->getIdUser()->getSchedules()
                ]
            )
            ->subject("{$registry->getIdUser()->getName()} est en retard!")
            ->htmlTemplate('email/delay.html.twig');

        $this->mailer->send($email);
    }

}
