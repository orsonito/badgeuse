<?php

namespace App\EventSubscriber;

use App\Repository\RegistryRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Twig\Environment;

class TwigEventSubscriber implements EventSubscriberInterface
{
    private $twig;
    private $registryRepository;

    public function __construct(Environment $twig, RegistryRepository $registryRepository)
    {
        $this->twig                 = $twig;
        $this->registryRepository   = $registryRepository;
    }

    public function onControllerEvent(ControllerEvent $event)
    {
        $this->twig->addGlobal('registries', $this->registryRepository->findAll());
    }

    public static function getSubscribedEvents()
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
    }
}
