<?php

namespace App\Controller;

use App\Entity\Registry;
use App\Entity\User;

use App\Service\RegistryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

// I USED SESSION HERE
use Symfony\Component\HttpFoundation\Session\Session;

class RegistryController extends AbstractController
{
    private $registryService;
    private $security;

    public function __construct(Security $security, RegistryService $registryService)
    {
        $this->security         = $security;
        $this->registryService  = $registryService;
    }

    #[Route('/registry', name: 'registry')]
    public function index(): Response
    {
        $user_repo  = $this->getDoctrine()->getRepository(User::class);
        $users      = $user_repo->findAll();

        return $this->render(
            'registry/index.html.twig', [
            'controller_name'   => 'RegistryController'
            ]
        );
    }

    #[Route('/registry/up/{id}', name: 'upregistry')]
    public function create(User $user): Response
    {
        $message = 'Your in !';

        if (!$this->registryService->create($user)) {
            $message = 'Its not possible doing that right now, try later';
        }

        // SESSION FLASH
        $session = new Session();
        $session->getFlashBag()->add('message', $message);

        return $this->redirectToRoute('index');
    }

    #[Route('/registry/{id}', name: 'detail')]
    public function read(Registry $registry): Response
    {
        if (!$registry) {
            return $this->redirectToRoute('user');
        }

        $idUserLogin    = $this->security->getUser();
        $idUserRegistry = $registry->getIdUser();

        if ($this->security->getUser()->getRoles()[0] == 'ROLE_ADMIN') {
            return $this->render(
                'registry/detail.html.twig', [
                'registry' => $registry
                ]
            );
        }

        if ($idUserLogin != $idUserRegistry) {
            return $this->redirectToRoute('home');
        }

        return $this->render(
            'registry/detail.html.twig', [
            'registry' => $registry
            ]
        );
    }

    #[Route('/registry/down/{id}', name: 'downregistry')]
    public function update(User $user): Response
    {
        $message = 'You are out !';

        if (!$this->registryService->update($user)) {
            $message = 'Its not possible doing that right now, try later';
        }

        // SESSION FLASH
        $session = new Session();
        $session->getFlashBag()->add('message', $message);

        return $this->redirectToRoute('index');
    }

    #[Route('/registry/delete/{id}', name: 'delete-registry')]
    public function delete(Registry $registry)
    {
        if ($this->security->getUser()->getRoles()[0] == 'ROLE_ADMIN') {
            $this->registryService->delete($registry);
        }

        return $this->redirectToRoute('home');
    }

    #[Route('/registry/all-my/{id}', name: 'registries')]
    public function getRegistriesByUser(User $user, Security $security): Response
    {
        if (!$user) {
            return $this->redirectToRoute('user');
        }

        $userLogin    = $security->getUser();
        $userRegistry = $user;

        if ($userLogin != $userRegistry) {
            return $this->redirectToRoute('user');
        }

        return $this->render(
            'registry/registries.html.twig', [
            'registries' => $user->getRegistries()
            ]
        );
    }
}
