<?php

namespace App\Controller;

use App\Form\ScheduleType;

// USE ENTITIES
use App\Entity\Schedule;
use App\Entity\User;

// USE SERVICES
use App\Service\ScheduleService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ScheduleController extends AbstractController
{
    private $scheduleService;
    private $security;

    public function __construct(ScheduleService $scheduleService, Security $security)
    {
        $this->scheduleService  = $scheduleService;
        $this->security         = $security;
    }

    #[Route('/schedule', name: 'schedule')]
    public function index(): Response
    {
        return $this->render(
            'schedule/index.html.twig', [
            'controller_name' => 'ScheduleController',
            ]
        );
    }

    #[Route('/schedule/create/{id}', name: 'create-schedule')]
    public function create(User $user, Request $request): Response
    {
        if ($this->getUser()->getRoles()[0] != 'ROLE_ADMIN') {
            if ($this->security->getUser() != $user) {
                return $this->redirectToRoute('home');
            }
        }

        // SESSION FLASH
        $schedule   = new Schedule();
        $form       = $this->createForm(ScheduleType::class, $schedule);

        // FILL OBJECT WITH FORM DATA
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entryTime  = $schedule->getEntryTime()->getTimestamp();
            $exitTime   = $schedule->getExitTime()->getTimestamp();

            if ($entryTime > $exitTime) {
                $message = 'Your entry cannot be more than your exit schedule!';
            } else if ($this->scheduleService->update($user, $schedule)) {
                $message = 'All right !';
            }

            $session    = new Session();
            $session->getFlashBag()->add('message', $message);
        }

        return $this->render(
            'schedule/create.html.twig', [
            'title'         => 'Create Schedule',
            'schedules'     => $user->getSchedules(),
            'scheduleDay'   => $schedule->getDay(),
            'form'          => $form->createView()
            ]
        );
    }
}