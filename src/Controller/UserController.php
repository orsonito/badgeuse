<?php

namespace App\Controller;

use App\Service\ScheduleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

// USE ENTITY
use App\Entity\User;

// USE CUSTOM FORMS
use App\Form\UserType;
use App\Form\LoginType;

// USE AUTHENTIC FOR LOGIN
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

// USE SERVICES
use App\Service\UserService;
use App\Service\RegistryService;

use Symfony\Component\Security\Core\Security;

class UserController extends AbstractController
{
    private $userService;
    private $registryService;
    private $security;

    public function __construct(
        UserService         $userService,
        RegistryService     $registryService,
        Security            $security
    ) {
        $this->registryService  = $registryService;
        $this->userService      = $userService;
        $this->security         = $security;
    }

    #[Route('/', name: 'home')]
    public function index(): Response
    {
        if ($this->security->getUser() && $this->security->getUser()->getRoles()[0] == 'ROLE_ADMIN') {
            return $this->redirectToRoute('admin');
        }

        $registries = $this->registryService->getRegistriesToday();

        return $this->render(
            'user/index.html.twig', [
            'registries' => $registries
            ]
        );
    }

    #[Route('/sign-up', name: 'sign-up')]
    public function create(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userService->create($user);

            $session = new Session();
            $session->getFlashBag()->add('message', 'User created!');

            return $this->redirectToRoute('login');
        }

        $title = 'Time to be part of something great';

        return $this->render(
            'user/sign-up.html.twig', [
            'title' => $title,
            'form'  => $form->createView(),
            'user'  => $user
            ]
        );
    }

    #[Route('/user/{id}', name: 'detail-user')]
    public function read(User $user): Response
    {
        if ($this->security->getUser()->getRoles()[0] != 'ROLE_ADMIN' && $user != $this->security->getUser()) {
            return $this->redirectToRoute('home');
        }

        return $this->render(
            'user/detail.html.twig', [
            'user'          => $user,
            'schedules'     => $user->getSchedules(),
            'scheduleDay'   => false,
            'registries'    => $user->getRegistries()
            ]
        );
    }

    #[Route('/user/update/{id}', name: 'update-user')]
    public function update(Request $request, User $user): Response
    {

        if ($this->security->getUser()->getRoles()[0] != 'ROLE_ADMIN') {
            if ($this->security->getUser() != $user) {
                return $this->redirectToRoute('home');
            }
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->userService->update($user)) {
                return $this->redirectToRoute('home');
            }
        }

        return $this->render(
            'user/sign-up.html.twig', [
            'title'     => 'Edit',
            'form'      => $form->createView()
            ]
        );
    }

    #[Route('/user/delete/{id}', name: 'delete-user')]
    public function delete(User $user): Response
    {
        if ($this->security->getUser()->getRoles()[0] == 'ROLE_ADMIN' || $user == $this->security->getUser()) {
            $this->userService->delete($user);
        }

        return $this->redirectToRoute('home');
    }

    // LOGIN
    #[Route('/login', name: 'login')]
    public function login(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        $user = new User();
        $form = $this->createForm(LoginType::class, $user);

        // FILL OBJECT WITH FORM DATA
        $form->handleRequest($request);

        $error          = $authenticationUtils->getLastAuthenticationError();
        $lastUserName   = $authenticationUtils->getLastUsername();

        return $this->render(
            'user/login.html.twig', array(
            'title'         => 'Log in',
            'error'         => $error,
            'form'          => $form->createView(),
            'last_username' => $lastUserName
            )
        );
    }
}
