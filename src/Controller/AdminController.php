<?php

namespace App\Controller;

use App\Service\RegistryService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// PDF
use Knp\Snappy\Pdf;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

class AdminController extends AbstractController
{
    private $registryService;
    private $userService;
    private $pdf;

    public function __construct(
        UserService     $userService,
        RegistryService $registryService,
        Pdf             $pdf
    ) {
        $this->registryService  = $registryService;
        $this->userService      = $userService;
        $this->pdf              = $pdf;
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $registries = $this->registryService->getRegistriesToday();

        $delays = false;
        foreach ($registries as $registry) {
            // 3600s    = 1 HEURE DE PLUS
            // 900s     = 15MIN POUR LE RETARD
            if ($registry->getDelayTime()->getTimestamp()+ 3600 > 900) {
                $delays[] =  $registry;
            }
        }

        return $this->render(
            'admin/index.html.twig', [
            'title'         => 'You got the power !',
            'delays'        => $delays,
            'registries'    => $registries
            ]
        );
    }

    #[Route('/admin/all-users', name: 'all-users')]
    public function allUser(): Response
    {
        $users      = $this->userService->getUsers();

        return $this->render(
            'user/users.html.twig', [
            'title' => 'All the users are right here !',
            'users' => $users
            ]
        );
    }

    #[Route('/admin/all-registries', name: 'all-registries')]
    public function allRegistry(): Response
    {
        $registries = $this->registryService->getRegistries();

        return $this->render(
            'registry/registries.html.twig', [
            'title' => 'You got the power !',
            'registries' => $registries
            ]
        );
    }

    #[Route('/admin/download-registries', name: 'download-registries')]
    public function downloadPdf(): response
    {
        $html = $this->renderView(
            'pdf/registries-today.html.twig',
            [
                'registries' => $this->registryService->getRegistriesToday()
            ]
        );

        $pdf = $this->pdf->getOutputFromHtml(
            $html
        );

        $response = new PdfResponse(
            $pdf,
            'registries.pdf'
        );

        $response->headers->remove('Cache-Control');

        return $response;
    }
}
