<?php

namespace App\Controller\Admin;

use App\Entity\Registry;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class RegistryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Registry::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
