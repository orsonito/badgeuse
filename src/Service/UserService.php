<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// ENCODER PASSWORD
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

// MY ENTITIES
use App\Entity\User;
use App\Entity\Schedule;

class UserService extends AbstractController
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder      = $encoder;
    }

    public function create(User $user): bool
    {
        $encoded = $this->encoder->encodePassword($user, $user->getPassword());
        $user->setRole('ROLE_USER');
        $user->setPassword($encoded);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        date_default_timezone_set('Europe/Paris');
        $defaultTime = date('Y-m-d 00:00:00');
        $defaultTime = new \DateTime($defaultTime);

        $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
        foreach ($days as $day) {
            $schedule   = new Schedule();
            $schedule->setDay($day);
            $schedule->setIdUser($user);
            $schedule->setEntryTime($defaultTime);
            $schedule->setExitTime($defaultTime);

            $em =$this->getDoctrine()->getManager();
            $em->persist($schedule);
            $em->flush();
        }

        return true;
    }

    public function read(User $user): object
    {
        $user_repo = $this->getDoctrine()->getRepository(User::class);
        return $user_repo->findOneBy(
            [
                'id'       =>  $user->getId()
            ]
        );
    }

    public function update(User $user): bool
    {
        $encoded = $this->encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encoded);
        $em =$this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return true;
    }

    public function delete(User $user): bool
    {
        $em =$this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return true;
    }

    public function getUsers(): array
    {
        $user_repo = $this->getDoctrine()->getRepository(User::class);
        return $user_repo->findAll();
    }
}
