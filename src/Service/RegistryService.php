<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// MY ENTITIES
use App\Entity\Registry;
use App\Entity\Schedule;
use App\Entity\User;

class RegistryService extends AbstractController
{
    public function create(User $user): bool
    {
        date_default_timezone_set('Europe/Paris');
        $now        = date('Y-m-d H:i:s');
        $now        = new \DateTime($now);
        $nowTime    = date('1970-01-01 H:i:s');
        $nowTime    = new \DateTime($nowTime);
        $weekDay    = strtolower(date('l'));

        $schedule_repo  = $this->getDoctrine()->getRepository(Schedule::class);
        $registry_repo     = $this->getDoctrine()->getRepository(Registry::class);

        // CONTROL FOR THE ACTUAL DAY
        $schedule = $schedule_repo->findOneBy(
            [
                                                    'day'       => $weekDay,
                                                    'id_user'   => $user
                                                ]
        );

        $nowTime    = $nowTime->getTimestamp();
        $entryTime  = $schedule->getEntryTime()->getTimestamp();

        // IF YOU ARE AN TIME TRAVELLER I DON'T LET YOU GO ON
        if ($nowTime < $entryTime) {
            return false;
        }

        $registred = $registry_repo->findOneBy(
            [
                                                    'date'      => $now,
                                                    'id_user'   => $user
                                                ]
        );

        // YOU CAN'T BEING HERE TWICE
        if ($registred) {
            return false;
        }

        $registry = new Registry();
        $registry->setIdUser($user);
        $registry->setDate($now);
        $registry->setEntryTime($now);
        $registry->setExitTime($now);

        $timeDelay = new \DateTime();
        $timeDelay->setTimestamp(($nowTime - $entryTime) - (60 * 60));
        $timeDelay = $timeDelay->format('Y-m-d H:i:s');
        $timeDelay = new \DateTime($timeDelay);
        $registry->setDelayTime($timeDelay);

        //$this->sendEmail($registry->getDelayTime()->getTimestamp());

        $timeTotal = date('Y-m-d 0:00:00');
        $timeTotal = new \DateTime($timeTotal);
        $registry->setTotalTime($timeTotal);

        $em = $this->getDoctrine()->getManager();
        $em->persist($registry);
        $em->flush();

        return true;
    }

    public function read(User $user): User
    {
        return $user;
    }

    public function update(User $user): bool
    {
        date_default_timezone_set('Europe/Paris');
        $now = date('Y-m-d H:i:s');
        $now = new \DateTime($now);

        $em             = $this->getDoctrine()->getManager();
        $registry_repo  = $em->getRepository(Registry::class);
        $registry       = $registry_repo->findOneBy(
            [
                'date'      => $now,
                'id_user'   => $user
            ]
        );

        if ($registry && $registry->getEntryTime() == $registry->getExitTime()) {
            $registry->setExitTime($now);

            $timediff   = $registry->getEntryTime()->diff($now);
            $totalTime  = date("Y-m-d $timediff->h:$timediff->i:$timediff->s");
            $totalTime  = new \DateTime($totalTime);
            $registry->setTotalTime($totalTime);

            $em->persist($registry);
            $em->flush();

            return true;
        }
        return false;
    }

    public function delete(Registry $registry): bool
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($registry);
        $em->flush();

        return true;
    }

    public function getRegistriesToday(): array
    {
        date_default_timezone_set('Europe/Paris');
        $now = date('Y-m-d H:i:s');
        $now = new \DateTime($now);

        $entry_repo = $this->getDoctrine()->getRepository(Registry::class);
        return $entry_repo->findBy(
            ['date' => $now],
            ['entry_time' => 'DESC']
        );
    }

    public function getRegistries(): array
    {
        $entry_repo = $this->getDoctrine()->getRepository(Registry::class);
        $registries = $entry_repo->findAll();

        return $registries;
    }
}
