<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// MY ENTITIES
use App\Entity\Schedule;
use App\Entity\User;

class ScheduleService extends AbstractController
{
    public function update(User $user, Schedule $schedule): bool
    {
        $em = $this->getDoctrine()->getManager();

        // SEARCH SCHEDULE DAY FOR UPDATE
        $schedule_repo  = $em->getRepository(Schedule::class);
        $scheduleDay    = $schedule_repo->findOneBy(
            [
                'day'       => $schedule->getDay(),
                'id_user'   => $user
            ]
        );

        $scheduleDay->setEntryTime($schedule->getEntryTime());
        $scheduleDay->setExitTime($schedule->getExitTime());

        $em->persist($scheduleDay);
        $em->flush();

        return true;
    }
}
