<?php

namespace App\Entity;

use App\Repository\RegistryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RegistryRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Registry
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="registries")
     */
    private $id_user;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="time")
     */
    private $entry_time;

    /**
     * @ORM\Column(type="time")
     */
    private $exit_time;

    /**
     * @ORM\Column(type="time")
     */
    private $delay_time;

    /**
     * @ORM\Column(type="time")
     */
    private $total_time;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?User
    {
        return $this->id_user;
    }

    public function setIdUser(?User $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getEntryTime(): ?\DateTimeInterface
    {
        return $this->entry_time;
    }

    public function setEntryTime(\DateTimeInterface $entry_time): self
    {
        $this->entry_time = $entry_time;

        return $this;
    }

    public function getExitTime(): ?\DateTimeInterface
    {
        return $this->exit_time;
    }

    public function setExitTime(\DateTimeInterface $exit_time): self
    {
        $this->exit_time = $exit_time;

        return $this;
    }

    public function getDelayTime(): ?\DateTimeInterface
    {
        return $this->delay_time;
    }

    public function setDelayTime(\DateTimeInterface $delay_time): self
    {
        $this->delay_time = $delay_time;

        return $this;
    }

    public function getTotalTime(): ?\DateTimeInterface
    {
        return $this->total_time;
    }

    public function setTotalTime(\DateTimeInterface $total_time): self
    {
        $this->total_time = $total_time;

        return $this;
    }
}
